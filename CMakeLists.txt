cmake_minimum_required(VERSION 2.8)

PROJECT(Slider)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "C:/git/vtk-8.2.0-install/natives-Windows-AMD64/cmake/vtk-8.2")
find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

add_executable(Slider Slider.cxx)

if(VTK_LIBRARIES)
  target_link_libraries(Slider ${VTK_LIBRARIES})
else()
  target_link_libraries(Slider vtkHybrid vtkWidgets)
endif()